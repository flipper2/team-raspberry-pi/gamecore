import flipperengine

engine = flipperengine.Engine()

letters_hit = []

multiplier = 1

def play_animation(animation_id):
    engine.set_item_state(f'animation_{animation_id}', 1)

@engine.on_item_state_changed('letter_p1', 1)
@engine.on_item_state_changed('letter_i', 1)
@engine.on_item_state_changed('letter_m', 1)
@engine.on_item_state_changed('letter_p2', 1)
def on_letter_hit(item_id, item_state):
    if item_id in letters_hit:
        return
    letters_hit.append(item_id)

    if len(letters_hit) == 4:
        if multiplier >= 2:
            return
        multiplier += 1
        play_animation(f'bonus_x{multiplier}_on')
        letters_hit = []
        # handle all letters hit here
    
@engine.on_item_state_changed('counter_player_0')
def on_score_changed(item_id, item_state):
    if item_state == 0:
        play_animation('level_reset')
        return
    
    level = int(item_state / 1000) % 10
    play_animation(f'level_set_{level}k')

engine.item_follow('flipper_left_outer', 'flipper_left_inner', item_source='button_left')
engine.item_follow('flipper_right_outer', 'flipper_right_inner', item_source='button_right')

@engine.on_item_state_changed('mushroom_left')
def handle(item_id, item_state):
    engine.item_proxy.counter_player_0 += x

engine.item_award('mushroom_left')
engine.item_award('mushroom_middle', awarding_amount=100)
engine.item_award('mushroom_right')

engine.item_award('gate_left')

print('gamecore started')
engine.loop_run()